package com.reger.datasource.core;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;

public abstract class ReferenceDataSource implements DataSource,InitializingBean {
	
	protected DataSource referenceDataSource;

	@Override
	public void afterPropertiesSet() throws Exception {
//		getRefDataSource();		
	}
	private DataSource getRefDataSource()  {
		if (referenceDataSource != null) {
			return referenceDataSource;
		}
		synchronized (this) {
			if (referenceDataSource != null) {
				return referenceDataSource;
			}
			referenceDataSource = getReferenceDataSource();
			return referenceDataSource;
		}
	}
	
	abstract DataSource getReferenceDataSource() ;
	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return getRefDataSource().getLogWriter();
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {
		getRefDataSource().setLogWriter(out);
	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {
		getRefDataSource().setLoginTimeout(seconds);
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return getRefDataSource().getLoginTimeout();
	}

	@Override
	public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return getRefDataSource().getParentLogger();
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return getRefDataSource().unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return getRefDataSource().isWrapperFor(iface);
	}

	@Override
	public Connection getConnection() throws SQLException {
		return getRefDataSource().getConnection();
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		return getRefDataSource().getConnection(username, password);
	}

}
